function handleNavLinkClick(event, href) {
  // Prevent default anchor click behavior
  event.preventDefault();

  // Check if the link is for a section on the current page
  const targetId = href.split("#")[1];
  const targetElement = document.getElementById(targetId);

  // Function to scroll to the target element
  function scrollToTarget() {
    if (targetElement) {
      // Smoothly scroll to the target element
      targetElement.scrollIntoView({ behavior: "smooth" });
      // Update the URL hash after scrolling
      window.history.pushState(null, null, "#" + targetId);
    } else {
      // If the target element is not on the same page, navigate to the URL
      window.location.href = href;
    }
  }

  // Check if the page is at the top
  if (window.scrollY !== 0) {
    // Scroll to the top of the page
    window.scrollTo({ top: 0, behavior: "smooth" });

    // Add an event listener to detect when the scrolling to the top has completed
    window.addEventListener("scroll", function onScroll() {
      if (window.scrollY === 0) {
        // Remove the event listener after reaching the top
        window.removeEventListener("scroll", onScroll);
        // Scroll to the target section
        scrollToTarget();
      }
    });
  } else {
    // Directly scroll to the target section if already at the top
    scrollToTarget();
  }
}

// JavaScript for displaying the popup with child prices
const priceInfoIcons = document.querySelectorAll(".tm-price-info");
const popup = document.getElementById("tm-popup");
const popupContent = document.getElementById("tm-popup-content");

priceInfoIcons.forEach((icon) => {
  icon.addEventListener("mouseover", () => {
    const info = icon.getAttribute("data-info");
    popupContent.innerText = info;

    const iconRect = icon.getBoundingClientRect();
    popup.style.display = "block";
    popup.style.left = iconRect.left + "px";
    popup.style.top = iconRect.top - popup.offsetHeight + "px";
  });

  icon.addEventListener("mouseout", () => {
    popup.style.display = "none";
  });
});

// popup.addEventListener("mouseover", () => {
//   popup.style.display = "block";
// });

// popup.addEventListener("mouseout", () => {
//   popup.style.display = "none";
// });

document.querySelectorAll(".tm-slideshow img").forEach(function (img) {
  img.addEventListener("click", function () {
    const slideshowSection = document.getElementById("tm-tour-slideshow");

    if (slideshowSection.requestFullscreen) {
      slideshowSection.requestFullscreen();
    } else if (slideshowSection.mozRequestFullScreen) {
      // Firefox
      slideshowSection.mozRequestFullScreen();
    } else if (slideshowSection.webkitRequestFullscreen) {
      // Chrome, Safari and Opera
      slideshowSection.webkitRequestFullscreen();
    } else if (slideshowSection.msRequestFullscreen) {
      // IE/Edge
      slideshowSection.msRequestFullscreen();
    } else if (slideshowSection.webkitEnterFullscreen) {
      // iOS Safari
      slideshowSection.webkitEnterFullscreen();
    } else {
      // Handle cases where the Fullscreen API is not supported
      alert("Your browser does not support fullscreen mode.");
    }
  });
});

document.addEventListener("DOMContentLoaded", function () {
  document.querySelectorAll(".tm-price-box").forEach(function (priceBox) {
    priceBox.addEventListener("click", function (event) {
      // Prevent event from bubbling up to the document click event
      event.stopPropagation();

      // Get the associated price info box
      var priceInfo = priceBox.querySelector(".tm-price-info");

      // If the clicked price info box is already active, close it
      if (priceInfo.classList.contains("active")) {
        priceInfo.classList.remove("active");
      } else {
        // Close any other open price info boxes
        document.querySelectorAll(".tm-price-info").forEach(function (infoBox) {
          infoBox.classList.remove("active");
        });

        // Open the clicked price info box
        priceInfo.classList.add("active");
      }
    });
  });

  // Close the price info box when clicking outside of it
  document.addEventListener("click", function (event) {
    if (!event.target.closest(".tm-price-box")) {
      document.querySelectorAll(".tm-price-info").forEach(function (infoBox) {
        infoBox.classList.remove("active");
      });
    }
  });
});
function redirectTo(url) {
  window.location.href = url;
}

document.addEventListener("DOMContentLoaded", function () {
  const translations = {};
  const langIcons = {
    en: "img/england.png",
    sr: "img/serbia.png",
    ru: "img/russia.png",
  };

  async function loadTranslations(language) {
    try {
      const response = await fetch(`${language}.json`);
      if (!response.ok) throw new Error("Network response was not ok");
      const data = await response.json();
      Object.assign(translations, data);
      updateText();
    } catch (error) {
      console.error("Error loading translations:", error);
    }
  }

  function updateText() {
    document.querySelectorAll("[data-i18n]").forEach((element) => {
      const key = element.getAttribute("data-i18n");
      if (translations[key]) {
        element.textContent = translations[key];
      }
    });

    document.querySelectorAll("[data-i18n-placeholder]").forEach((element) => {
      const key = element.getAttribute("data-i18n-placeholder");
      if (translations[key]) {
        element.setAttribute("placeholder", translations[key]);
      }
    });
  }

  function updateLangIcon(language) {
    const iconElement = document.getElementById("current-lang-icon");
    if (iconElement) {
      iconElement.src = langIcons[language];
    } else {
      console.error("Icon element not found");
    }
  }

  function setLanguage(language) {
    localStorage.setItem("currentLanguage", language);
    loadTranslations(language);
    updateLangIcon(language);
  }

  // Set initial language
  const currentLanguage = localStorage.getItem("currentLanguage") || "en";
  setLanguage(currentLanguage);

  // Event listener for language selection
  document.querySelectorAll(".dropdown-item").forEach((item) => {
    item.addEventListener("click", (event) => {
      event.preventDefault();
      const language = item.getAttribute("data-lang");
      setLanguage(language);
    });
  });
});
